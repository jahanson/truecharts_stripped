

## [minio-5.0.18](https://github.com/truecharts/charts/compare/minio-5.0.17...minio-5.0.18) (2023-02-06)

### Chore

- update container image tccr.io/truecharts/minio to latest
  
  