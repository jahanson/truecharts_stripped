

## [minio-5.0.15](https://github.com/truecharts/charts/compare/minio-console-5.0.13...minio-5.0.15) (2023-01-18)

### Chore

- update container image tccr.io/truecharts/minio to latest
  
  