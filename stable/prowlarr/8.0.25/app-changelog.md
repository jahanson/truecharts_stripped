

## [prowlarr-8.0.25](https://github.com/truecharts/charts/compare/prowlarr-8.0.24...prowlarr-8.0.25) (2023-02-20)

### Chore

- update container image tccr.io/truecharts/prowlarr to v1.2.2.2699
  
  