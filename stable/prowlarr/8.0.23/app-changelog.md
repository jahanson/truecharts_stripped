

## [prowlarr-8.0.23](https://github.com/truecharts/charts/compare/prowlarr-8.0.22...prowlarr-8.0.23) (2023-02-08)

### Chore

- update container image tccr.io/truecharts/prowlarr to 1.1.3.2521
  
  