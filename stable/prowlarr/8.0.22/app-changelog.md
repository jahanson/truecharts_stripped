

## [prowlarr-8.0.22](https://github.com/truecharts/charts/compare/prowlarr-8.0.21...prowlarr-8.0.22) (2023-02-07)

### Chore

- update container image tccr.io/truecharts/prowlarr to 1.1.3.2521
  
  