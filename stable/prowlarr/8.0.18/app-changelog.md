

## [prowlarr-8.0.18](https://github.com/truecharts/charts/compare/prowlarr-8.0.17...prowlarr-8.0.18) (2023-01-19)

### Chore

- update container image tccr.io/truecharts/prowlarr to v1.1.0.2322
  
  